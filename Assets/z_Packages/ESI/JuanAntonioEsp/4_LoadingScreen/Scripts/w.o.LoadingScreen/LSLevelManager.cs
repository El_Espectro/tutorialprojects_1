using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LSLevelManager : MonoBehaviour {
	
    public void LoadLevel(string levelName) {
        StartCoroutine(LoadLevelAsync(levelName));
    }

    private IEnumerator LoadLevelAsync(string levelName) {
        AsyncOperation operation = SceneManager.LoadSceneAsync(levelName);

        while (!operation.isDone) {
            float progress = Mathf.Clamp01(operation.progress);

            Debug.Log($"Cargando escena: {progress}");

            yield return null;
        }
    }
}
