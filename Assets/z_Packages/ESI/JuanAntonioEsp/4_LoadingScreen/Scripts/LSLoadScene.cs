using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LSLoadScene : MonoBehaviour {

    /*[field: SerializeField] public string targetScene { get; set; }
	
    public void LoadScene() {
        LSLoadingData.sceneToLoad = targetScene;
        Load();
    }*/

    public void LoadScene(string nextScene) {
        LSLoadingData.sceneToLoad = nextScene;
        Load();
    }

    private static void Load() {
        LSLoadingData.previousScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene("LS_LoadingScreen");
    }
}
