using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LSLoadingScreenManager : MonoBehaviour {

    [SerializeField] Image progressBar;
    private Text progressText;
	
    // Start is called before the first frame update
    void Start() {
        progressText = progressBar.gameObject.GetComponentInChildren<Text>();
        StartCoroutine(LoadSceneAsync());
    }

    private IEnumerator LoadSceneAsync() {
        AsyncOperation operation = SceneManager.LoadSceneAsync(LSLoadingData.sceneToLoad);
        operation.allowSceneActivation = false;

        while (!operation.isDone) {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            progressBar.fillAmount = progress;
            progressText.text = progress.ToString("P");

            //Aqu� se pueden mostrar consejos u hacer otras cosas en la escena.

            if (operation.progress >= 0.9f) {
                operation.allowSceneActivation = true;
            }

            yield return null;
        }
    }
}
