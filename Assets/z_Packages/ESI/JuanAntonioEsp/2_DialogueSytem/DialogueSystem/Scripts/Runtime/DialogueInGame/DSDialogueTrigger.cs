using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DSDialogueTrigger : MonoBehaviour {

    private DSDialogueManager DialogueManager;
    [field: SerializeField] public DSDialogueSO startingDialogue { get; private set; }

    [field: HideInInspector] public bool alreadyPlayed;

    // Start is called before the first frame update
    void Start() {
        DialogueManager = (DSDialogueManager) GameObject.FindObjectOfType(typeof(DSDialogueManager));
    }

    // Update is called once per frame
    void Update() {
        
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player") && !alreadyPlayed) {
            DialogueManager.SetDialogue(startingDialogue, this);
            DialogueManager.ShowText();
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")) {
            if (DialogueManager.playingDialogue)
                DialogueManager.StopDialogue();
        }
    }
}
