using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

[Serializable]
[RequireComponent(typeof(DSDialogueActions))]
public class DSDialogueManager : MonoBehaviour {

    DSDialogueActions inputActions;

    private DSDialogueSO startingDialogue { get; set; }
    [field: SerializeField] public Text textUI { get; private set; }
    [field: SerializeField] public GameObject choiceButtonPrefab { get; private set; }

    private DSDialogueSO currentDialogue;
    private DSDialogueTrigger currentTrigger;

    private List<GameObject> buttonsToDelete;
    [HideInInspector] public bool playingDialogue;

    private bool onLastDialogue;

    private void Start() {
        inputActions = GetComponent<DSDialogueActions>();

        //currentDialogue = startingDialogue;
        //Debug.Log(startingDialogue);
        //textUI = GameObject.Find("DialogueText").GetComponent<Text>();
        //ShowText();
    }

    public void SetDialogue(DSDialogueSO startingDialogue, DSDialogueTrigger dialogueTrigger) {
        this.startingDialogue = startingDialogue;
        currentDialogue = this.startingDialogue;

        currentTrigger = dialogueTrigger;
    }

    public void ShowText() {
        AddInputActionsCallbacks();
        playingDialogue = true;
        textUI.gameObject.SetActive(true);

        //Debug.Log("Antes del texto");
        textUI.text = currentDialogue.Text;

        Debug.Log($"Num choices in {currentDialogue.DialogueName}: {currentDialogue.Choices.Count}");
        //Debug.Log($"Next choice in {currentDialogue.DialogueName} is {currentDialogue.Choices[0].NextDialogue.DialogueName}");

        if (currentDialogue.Choices.Count == 0 || currentDialogue.Choices[0].NextDialogue == null) {
            Debug.Log($"Este �ltimo di�logo es: {currentDialogue.DialogueName}");
            StartCoroutine(ShowLastDialogue());
            return;
        }

        if (currentDialogue.DialogueType == DSDialogueType.SingleChoice) {
            //AddInputActionsCallbacks();
            return;
        }

        StartCoroutine(ChoicesGenerator());
    }

    private void NextDialogue(InputAction.CallbackContext context) {
        if (onLastDialogue)
            return;
        //RemoveInputActionsCallbacks();
        currentDialogue = currentDialogue.Choices[0].NextDialogue;
        ShowText();
    }

    #region Choice Methods
    private IEnumerator ChoicesGenerator() {

        float yPosition = -80f;
        //DSDialogueSO nextDialogue;
        buttonsToDelete = new List<GameObject>();

        foreach (DSDialogueChoiceData choice in currentDialogue.Choices) {
            GameObject prefab = Instantiate(choiceButtonPrefab, textUI.gameObject.transform);

            prefab.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, yPosition, 0f);

            yPosition -= 65f;

            Button button = prefab.GetComponent<Button>();

            prefab.GetComponentInChildren<Text>().text = choice.Text;
            button.onClick.AddListener(() => SelectedChoice(choice.NextDialogue));

            //nextDialogue = choice.NextDialogue;
            buttonsToDelete.Add(prefab);
        }

        yield return new WaitForSeconds(0f);
        //OnOptionChosen()
    }

    private void SelectedChoice(DSDialogueSO nextDialogue) {

        if (nextDialogue == null) {
            currentTrigger.alreadyPlayed = true;
            StopDialogue();
            //currentDialogue = startingDialogue;
            return;
        }

        currentDialogue = nextDialogue;

        ClearButtons();

        ShowText();
    }

    private void ClearButtons() {
        foreach (GameObject button in buttonsToDelete)
            Destroy(button);

        buttonsToDelete.Clear();
    }
    #endregion

    private IEnumerator ShowLastDialogue() {
        onLastDialogue = true;
        yield return new WaitUntil(() => inputActions.DialogueActions.Dialogo.IsPressed());
        currentTrigger.alreadyPlayed = true;
        StopDialogue();
    }

    public void StopDialogue() {
        //ToDo: Stop Dialogue completely.

        playingDialogue = false;
        onLastDialogue = false;
        currentDialogue = startingDialogue;
        textUI.gameObject.SetActive(false);

        if (buttonsToDelete != null)
            if (buttonsToDelete.Count != 0)
                ClearButtons();

        StopAllCoroutines();
        RemoveInputActionsCallbacks();
    }

    private void AddInputActionsCallbacks() {
        inputActions.DialogueActions.Dialogo.performed += NextDialogue;
    }
    private void RemoveInputActionsCallbacks() {
        inputActions.DialogueActions.Dialogo.performed -= NextDialogue;
    }

    private void OnDisable() {
        RemoveInputActionsCallbacks();
    }

    #region Deprecated Button Callback Method
    /*private void OnOptionChosen(*//*int choiceIndex, *//*DSDialogueSO nextDialogue) {

        //nextDialogue = currentDialogue.Choices[choiceIndex].NextDialogue;

        if (nextDialogue == null) {
            currentDialogue = startingDialogue;
            StopDialogue();
            return;
        }

        currentDialogue = nextDialogue;

        ShowText();
    }*/
    #endregion
}
