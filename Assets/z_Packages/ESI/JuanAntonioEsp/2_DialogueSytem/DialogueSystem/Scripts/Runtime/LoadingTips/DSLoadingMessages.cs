using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(DSDialogueActions))]
public class DSLoadingMessages : MonoBehaviour {

    DSDialogueActions inputActions;

    /*[field: HideInInspector] */
    [field: SerializeField] private DSDialogueSO startingDialogue { get; set; }
    [field: SerializeField] public Text textUI { get; set; }

    [field: SerializeField] private float maxTipTime { get; set; } = 5f;
    //private float tipTime;

    private DSDialogueSO currentDialogue;

    private void Start() {
        inputActions = GetComponent<DSDialogueActions>();
        currentDialogue = startingDialogue;

        //tipTime = maxTipTime;

        //ShowText();
        StartCoroutine(ShowTip());
        AddInputActionsCallbacks();
    }

    public void ShowText() {
        textUI.gameObject.SetActive(true);

        textUI.text = currentDialogue.Text;

        //Debug.Log("Num choices: " + currentDialogue.Choices.Count);

    }

    private void Next(InputAction.CallbackContext context) {
        //RemoveInputActionsCallbacks();
        StopAllCoroutines();
        NextTip();
        StartCoroutine(ShowTip());
    }

    private void NextTip() {
        if (currentDialogue.Choices.Count == 0 || currentDialogue.Choices[0].NextDialogue == null) {
            currentDialogue = startingDialogue;
        }
        else
            currentDialogue = currentDialogue.Choices[0].NextDialogue;
        //ShowText();
    }

    private IEnumerator ShowTip() {
        ShowText();

        yield return new WaitForSeconds(maxTipTime);

        NextTip();
        StartCoroutine(ShowTip());
    }

    private void AddInputActionsCallbacks() {
        inputActions.DialogueActions.Dialogo.performed += Next;
    }
    private void RemoveInputActionsCallbacks() {
        inputActions.DialogueActions.Dialogo.performed -= Next;
    }

    private void OnDisable() {
        StopAllCoroutines();
        RemoveInputActionsCallbacks();
        //Debug.Log("Tips disabled.");
    }
}
