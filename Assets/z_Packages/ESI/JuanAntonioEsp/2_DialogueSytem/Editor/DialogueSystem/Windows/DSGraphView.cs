using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

public class DSGraphView : GraphView {

    private DSEditorWindow editorWindow;
    private DSSearchWindow searchWindow;

    private MiniMap miniMap;

    private SerializableDictionary<string, DSNodeErrorData> ungroupedNodes;
    private SerializableDictionary<string, DSGroupErrorData> groups;
    private SerializableDictionary<Group, SerializableDictionary<string, DSNodeErrorData>> groupedNodes;

    private int nameErrorsAmount;

    public int NameErrorsAmount {
        get {
            return nameErrorsAmount;
        }
        set {
            nameErrorsAmount = value;

            if (nameErrorsAmount == 0)
                editorWindow.EnableSaving();

            if (nameErrorsAmount == 1)
                editorWindow.DisableSaving();
        }
    }

    public DSGraphView(DSEditorWindow editorWindow) {
        this.editorWindow = editorWindow;

        ungroupedNodes = new SerializableDictionary<string, DSNodeErrorData>();
        groups = new SerializableDictionary<string, DSGroupErrorData>();
        groupedNodes = new SerializableDictionary<Group, SerializableDictionary<string, DSNodeErrorData>>();

        AddManipulators();
        AddSearchWindow();
        AddGridBackground();
        AddMinimap();

        OnElementsDeleted();
        OnGroupElementsAdded();
        OnGroupElementsRemoved();
        OnGroupRenamed();

        OnGraphViewChanged();

        AddStyles();
        AddMinimapStyles();
    }

    #region Overrided Methods
    public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter) {
        List<Port> compatiblePorts = new List<Port>();

        ports.ForEach(port => {
            if (startPort == port || startPort.node == port.node)
                return;

            if (startPort.direction == port.direction)
                return;

            compatiblePorts.Add(port);

        });

        return compatiblePorts;
    }
    #endregion

    #region Manipulators Methods
    private void AddManipulators() {
        SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);

        this.AddManipulator(new ContentDragger());
        this.AddManipulator(new SelectionDragger());
        this.AddManipulator(new RectangleSelector());

        this.AddManipulator(CreateNodeContextualMenu(1, "Add Node (Single Choice)", DSDialogueType.SingleChoice));
        this.AddManipulator(CreateNodeContextualMenu(2, "Add Node (Multiple Choice)", DSDialogueType.MultipleChoice));

        this.AddManipulator(CreateGroupContextualMenu());
    }

    private IManipulator CreateGroupContextualMenu() {
        ContextualMenuManipulator contextualMenuManipulator = new ContextualMenuManipulator(
            /*menuEvent => menuEvent.menu.AppendAction("Add Group",
            actionEvent => CreateGroup("DialogueGroup", GetLocalMousePosition(actionEvent.eventInfo.localMousePosition)))*/

            menuEvent => menuEvent.menu.InsertAction(3, "Add Group",
            actionEvent => CreateGroup("DialogueGroup", GetLocalMousePosition(actionEvent.eventInfo.localMousePosition)))
        );

        return contextualMenuManipulator;
    }

    private IManipulator CreateNodeContextualMenu(int index, string actionTitle, DSDialogueType dialogueType) {
        ContextualMenuManipulator contextualMenuManipulator = new ContextualMenuManipulator(
            /*menuEvent => menuEvent.menu.AppendAction(actionTitle, 
            actionEvent => AddElement(CreateNode(dialogueType, GetLocalMousePosition(actionEvent.eventInfo.localMousePosition))))*/

            menuEvent => menuEvent.menu.InsertAction(index, actionTitle, 
            actionEvent => AddElement(CreateNode("DialogueName", dialogueType, GetLocalMousePosition(actionEvent.eventInfo.localMousePosition))))
        );

        return contextualMenuManipulator;
    }
    #endregion

    #region Elements Creation
    public DSGroup CreateGroup(string title, Vector2 position) {
        DSGroup group = new DSGroup(title, position);

        AddGroup(group);
        AddElement(group);

        foreach (GraphElement selectedElement in selection) {
            if (!(selectedElement is DSNode))
                continue;

            DSNode node = (DSNode)selectedElement;
            group.AddElement(node);
        }

        return group;
    }

    public DSNode CreateNode(string nodeName, DSDialogueType dialogueType, Vector2 position, bool shouldDraw = true) {
        Type nodeType = Type.GetType($"DS{dialogueType}Node");

        DSNode node = (DSNode)Activator.CreateInstance(nodeType);

        node.Initialize(nodeName, this, position);

        if (shouldDraw)
            node.Draw();

        AddUngroupedNode(node);

        return node;
    }
    #endregion

    #region Callbacks
    private void OnElementsDeleted() {
        deleteSelection = (operationName, askUser) => {
            Type groupType = typeof(DSGroup);
            Type edgeType = typeof(Edge);

            List<DSGroup> groupsToDelete = new List<DSGroup>();
            List<Edge> edgesToDelete = new List<Edge>();
            List<DSNode> nodesToDelete = new List<DSNode>();
            foreach (GraphElement element in selection) {
                if (element is DSNode node) {
                    nodesToDelete.Add(node);
                    continue;
                }

                if (element.GetType() == edgeType) {
                    Edge edge = (Edge)element;
                    edgesToDelete.Add(edge);
                    continue;
                }

                if (element.GetType() != groupType)
                    continue;
                

                DSGroup group = (DSGroup)element;

                groupsToDelete.Add(group);
            }

            foreach (DSGroup group in groupsToDelete) {
                List<DSNode> groupNodes = new List<DSNode>();

                foreach (GraphElement groupElement in group.containedElements) {
                    if (!(groupElement is DSNode))
                        continue;

                    DSNode groupNode = (DSNode)groupElement;
                    groupNodes.Add(groupNode);
                }

                group.RemoveElements(groupNodes);

                RemoveGroup(group);

                RemoveElement(group);
            }

            DeleteElements(edgesToDelete);

            foreach (DSNode nodeToDelete in nodesToDelete) {
                if (nodeToDelete.Group != null)
                    nodeToDelete.Group.RemoveElement(nodeToDelete);

                nodeToDelete.DisconectAllPorts();

                RemoveUngroupedNode(nodeToDelete);

                RemoveElement(nodeToDelete);
            }

            /*int count = selection.Count;

            for (int i = count - 1; i >= 0; i--) {
                if (selection[i] is DSNode node) {
                    if (node.Group != null)
                        node.Group.RemoveElement(node);
                    RemoveUngroupedNode(node);
                    RemoveElement(node);
                }

                if (selection[i].GetType() != groupType) {
                    continue;
                }

                DSGroup group = (DSGroup)selection[i];

                RemoveGrouped(group);
                RemoveElement(group);
            }*/
        };
    }
    private void OnGroupElementsAdded() {
        elementsAddedToGroup = (group, elements) => {
            foreach (GraphElement element in elements) {
                if (!(element is DSNode)) {
                    continue;
                }

                DSGroup nodeGroup = (DSGroup)group;

                DSNode node = (DSNode)element;
                RemoveUngroupedNode(node);

                AddGroupedNode(node, nodeGroup);
            }
        };
    }
    private void OnGroupElementsRemoved() {
        elementsRemovedFromGroup = (group, elements) => {
            foreach (GraphElement element in elements) {
                if (!(element is DSNode)) {
                    continue;
                }

                DSNode node = (DSNode)element;

                RemoveGroupedNode(node, group);
                AddUngroupedNode(node);
            }
        };
    }
    private void OnGroupRenamed() {
        groupTitleChanged = (group, newTitle) => {
            DSGroup dsGroup = (DSGroup)group;
            dsGroup.title = newTitle.RemoveWhitespaces().RemoveSpecialCharacters();

            if (string.IsNullOrEmpty(dsGroup.title)) {
                if (!string.IsNullOrEmpty(dsGroup.OldTitle))
                    NameErrorsAmount++;
            }
            else {
                if (string.IsNullOrEmpty(dsGroup.OldTitle))
                    NameErrorsAmount--;
            }

            RemoveGroup(dsGroup);

            dsGroup.OldTitle = dsGroup.title;

            AddGroup(dsGroup);
        };
    }
    private void OnGraphViewChanged() {
        graphViewChanged = (changes) => {
            if (changes.edgesToCreate != null) {
                foreach (Edge edge in changes.edgesToCreate) {
                    DSNode nextNode = (DSNode)edge.input.node;
                    DSChoiceSaveData choiceData = (DSChoiceSaveData)edge.output.userData;

                    choiceData.NodeID = nextNode.ID;
                }
            }

            if (changes.elementsToRemove != null) {
                Type edgeType = typeof(Edge);

                foreach (GraphElement element in changes.elementsToRemove) {
                    if (element.GetType() != edgeType)
                        continue;

                    Edge edge = (Edge)element;

                    DSChoiceSaveData choiceData = (DSChoiceSaveData)edge.output.userData;

                    choiceData.NodeID = "";
                }
            }

            return changes;
        };
    }
    #endregion

    #region Repeated Elements
    public void AddUngroupedNode(DSNode node) {
        string nodeName = node.DialogueName.ToLower();

        if (!ungroupedNodes.ContainsKey(nodeName)) {
            DSNodeErrorData nodeErrorData = new DSNodeErrorData();
            nodeErrorData.Nodes.Add(node);

            ungroupedNodes.Add(nodeName, nodeErrorData);

            return;
        }

        //List<DSNode> ungroupedNodesList = ungroupedNodes[nodeName].Nodes;

        DSNodeErrorData ungroupedNode = ungroupedNodes[nodeName];

        ungroupedNode.Nodes.Add(node);

        Color errorColor = ungroupedNode.ErrorData.Color;

        node.SetErrorStyle(errorColor);

        if (ungroupedNode.Nodes.Count == 2) {
            NameErrorsAmount++;
            ungroupedNode.Nodes[0].SetErrorStyle(errorColor);
        }
    }

    public void RemoveUngroupedNode(DSNode node) {
        string nodeName = node.DialogueName.ToLower();

        DSNodeErrorData ungroupedNode = ungroupedNodes[nodeName];
        ungroupedNode.Nodes.Remove(node);

        node.ResetStyle();

        if (ungroupedNode.Nodes.Count == 1) {
            NameErrorsAmount--;
            ungroupedNode.Nodes[0].ResetStyle();
            return;
        }

        if (ungroupedNode.Nodes.Count == 0) {
            ungroupedNodes.Remove(nodeName);
        }
    }

    public void AddGroup(DSGroup group) {
        string groupName = group.title.ToLower();

        if (!groups.ContainsKey(groupName)) {
            DSGroupErrorData groupErrorData = new DSGroupErrorData();

            groupErrorData.Groups.Add(group);

            groups.Add(groupName, groupErrorData);

            return;
        }

        DSGroupErrorData currentGroup = groups[groupName];

        currentGroup.Groups.Add(group);

        Color errorColor = currentGroup.ErrorData.Color;

        group.SetErrorStyle(errorColor);

        if (currentGroup.Groups.Count == 2) {
            NameErrorsAmount++;
            currentGroup.Groups[0].SetErrorStyle(errorColor);
        }
    }

    private void RemoveGroup(DSGroup group) {
        string oldGroupName = group.OldTitle.ToLower();

        List<DSGroup> groupList = groups[oldGroupName].Groups;

        groupList.Remove(group);

        group.ResetStyle();

        if (groupList.Count == 1) {
            NameErrorsAmount--;
            groupList[0].ResetStyle();
            return;
        }
        if (groupList.Count == 0)
            groups.Remove(oldGroupName);

    }

    public void AddGroupedNode(DSNode node, DSGroup group) {
        string nodeName = node.DialogueName.ToLower();

        node.Group = group;

        if (!groupedNodes.ContainsKey(group)) {
            groupedNodes.Add(group, new SerializableDictionary<string, DSNodeErrorData>());
        }

        SerializableDictionary<string, DSNodeErrorData> groupedNode = groupedNodes[group];

        if (!groupedNode.ContainsKey(nodeName)) {
            DSNodeErrorData nodeErrorData = new DSNodeErrorData();
            nodeErrorData.Nodes.Add(node);

            groupedNode.Add(nodeName, nodeErrorData);

            return;
        }

        groupedNode[nodeName].Nodes.Add(node);

        Color errorColor = groupedNode[nodeName].ErrorData.Color;

        node.SetErrorStyle(errorColor);

        if (groupedNode[nodeName].Nodes.Count == 2) {
            NameErrorsAmount++;
            groupedNode[nodeName].Nodes[0].SetErrorStyle(errorColor);
        }
    }

    public void RemoveGroupedNode(DSNode node, Group group) {
        // Remove node from its group.
        string nodeName = node.DialogueName.ToLower();

        node.Group = null;

        List<DSNode> groupedNodesList = groupedNodes[group][nodeName].Nodes;

        groupedNodesList.Remove(node);

        node.ResetStyle();

        if (groupedNodesList.Count == 1) {
            NameErrorsAmount--;
            groupedNodesList[0].ResetStyle();
            return;
        }
        if (groupedNodesList.Count == 0) {
            groupedNodes[group].Remove(nodeName);

            if (groupedNodes[group].Count == 0)
                groupedNodes.Remove(group);
        }

    }
    #endregion

    #region Elements Addition
    private void AddSearchWindow() {
        if (searchWindow == null) {
            searchWindow = ScriptableObject.CreateInstance<DSSearchWindow>();
            searchWindow.Initialize(this);
        }

        nodeCreationRequest = context => SearchWindow.Open(new SearchWindowContext(context.screenMousePosition), searchWindow);
    }

    private void AddMinimap() {
        miniMap = new MiniMap() {
            anchored = true
        };

        miniMap.SetPosition(new Rect(15, 50, 200, 180));

        Add(miniMap);

        miniMap.visible = false;
    }

    private void AddStyles() {
        this.AddStyleSheets(
            $"{DSElementUtility.styleFolderPath}DSGraphViewStyles.uss",
            $"{DSElementUtility.styleFolderPath}DSNodeStyles.uss"
        );
    }

    private void AddGridBackground() {
        GridBackground gridBackground = new GridBackground();
        gridBackground.StretchToParentSize();

        Insert(0, gridBackground);
    }

    private void AddMinimapStyles() {
        StyleColor backgroundColor = new StyleColor(new Color32(29, 29, 30, 255));
        StyleColor borderColor = new StyleColor(new Color32(51, 51, 51, 255));

        miniMap.style.backgroundColor = backgroundColor;
        miniMap.style.borderTopColor = borderColor;
        miniMap.style.borderRightColor = borderColor;
        miniMap.style.borderBottomColor = borderColor;
        miniMap.style.borderLeftColor = borderColor;
    }
    #endregion

    #region Utilities
    public Vector2 GetLocalMousePosition(Vector2 mousePosition, bool isSearchWindow=false) {
        Vector2 worldMousePosition = mousePosition;

        if (isSearchWindow)
            worldMousePosition -= editorWindow.position.position;

        Vector2 localMousePosition = contentViewContainer.WorldToLocal(worldMousePosition);

        return localMousePosition;
    }
    public void ClearGraph() {
        graphElements.ForEach(graphElement => RemoveElement(graphElement));

        groups.Clear();
        groupedNodes.Clear();
        ungroupedNodes.Clear();

        NameErrorsAmount = 0;
    }
    public void ToggleMinimap() {
        miniMap.visible = !miniMap.visible;
    }
    #endregion
}
