using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class DSEditorWindow : EditorWindow {

    private DSGraphView graphView;
    private static DSEditorWindow dialogueWindow;

    private readonly string defaultFileName = "DialoguesFileName";

    private Button saveButton;
    private Button minimapButton;
    private static TextField fileNameTextField;

    [MenuItem("Tools/Dialogue System/Dialogue Graph")]
    public static void Open() {
        dialogueWindow = GetWindow<DSEditorWindow>("Dialogue Graph");
        //Debug.Log("Dialogue Graph opened.");
    }

    private void CreateGUI() {
        AddGraphView();

        AddToolbar();
        AddStyles();
    }

    /*private void OnLostFocus() {
        dialogueWindow.Close();
    }*/

    #region Elements Addition
    private void AddGraphView() {
        graphView = new DSGraphView(this);

        graphView.StretchToParentSize();

        rootVisualElement.Add(graphView);
    }

    private void AddToolbar() {
        Toolbar toolbar = new Toolbar();

        fileNameTextField = DSElementUtility.CreateTextField(defaultFileName, "File Name:", callback => {
            fileNameTextField.value = callback.newValue.RemoveWhitespaces().RemoveSpecialCharacters();
        });

        saveButton = DSElementUtility.CreateButton("Save", () => Save());

        Button loadButton = DSElementUtility.CreateButton("Load", () => Load());
        Button clearButton = DSElementUtility.CreateButton("Clear", () => Clear());
        Button resetButton = DSElementUtility.CreateButton("Reset", () => ResetGraph());
        Button debugButton = DSElementUtility.CreateButton("Debug", () => DebugLog());

        minimapButton = DSElementUtility.CreateButton("Minimap", () => ToggleMinimap());

        toolbar.Add(fileNameTextField);
        toolbar.Add(saveButton);
        toolbar.Add(loadButton);
        toolbar.Add(clearButton);
        toolbar.Add(resetButton);
        toolbar.Add(minimapButton);
        toolbar.Add(debugButton);

        toolbar.AddStyleSheets($"{DSElementUtility.styleFolderPath}DSToolbarStyles.uss");

        rootVisualElement.Add(toolbar);
    }

    private void AddStyles() {
        rootVisualElement.AddStyleSheets($"{DSElementUtility.styleFolderPath}DSVariables.uss");
    }
    #endregion

    #region Toolbar Actions
    private void Save() {
        if (string.IsNullOrEmpty(fileNameTextField.value)) {
            EditorUtility.DisplayDialog(
                "Nombre de archivo no valido.",
                "Por favor, asegurese de que el nombre de archivo sea valido.",
                "Capisco"
            );

            return;
        }

        DSIOUtility.Initialize(graphView, fileNameTextField.value);
        DSIOUtility.Save();
    }
    private void Load() {
        string filePath = EditorUtility.OpenFilePanel("Dialogue Graph", $"{DSIOUtility.editorFolderPath}/Graphs", "asset");

        if (string.IsNullOrEmpty(filePath))
            return;

        Clear();

        DSIOUtility.Initialize(graphView, Path.GetFileNameWithoutExtension(filePath));
        DSIOUtility.Load();
    }
    private void Clear() {
        graphView.ClearGraph();
    }
    private void ResetGraph() {
        Clear();
        UpdateFileName(defaultFileName);
    }
    private void ToggleMinimap() {
        graphView.ToggleMinimap();
        minimapButton.ToggleInClassList("ds-toolbar__button__selected");
    }
    private void DebugLog() {
        Debug.Log(graphView.NameErrorsAmount);
    }
    #endregion

    #region Utility Methods
    public static void UpdateFileName(string newFileName) {
        fileNameTextField.value = newFileName;
    }
    public void EnableSaving() {
        saveButton.SetEnabled(true);
    }
    public void DisableSaving() {
        saveButton.SetEnabled(false);
    }
    #endregion
}
