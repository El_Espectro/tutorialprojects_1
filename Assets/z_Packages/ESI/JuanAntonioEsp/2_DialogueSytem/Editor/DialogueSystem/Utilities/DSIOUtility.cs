using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public static class DSIOUtility {

    private static DSGraphView graphView;

    private static string graphFileName;
    private static string containerFolderPath;
    public static string dsFatherFolderPath = "Assets/z_Packages/ESI/JuanAntonioEsp/2_DialogueSytem";
    public static string editorFolderPath = $"{dsFatherFolderPath}/Editor/DialogueSystem";
    public static string dsFolderPath = $"{dsFatherFolderPath}/DialogueSystem";

    private static List<DSGroup> groups;
    private static List<DSNode> nodes;

    private static Dictionary<string, DSDialogueGroupSO> createdDialogueGroups;
    private static Dictionary<string, DSDialogueSO> createdDialogues;

    private static Dictionary<string, DSGroup> loadedGroups;
    private static Dictionary<string, DSNode> loadedNodes;

    public static void Initialize(DSGraphView dsGraphView, string graphName) {
        graphView = dsGraphView;
        graphFileName = graphName;
        containerFolderPath = $"{dsFolderPath}/DialoguesSave/{graphFileName}";

        groups = new List<DSGroup>();
        nodes = new List<DSNode>();

        createdDialogueGroups = new Dictionary<string, DSDialogueGroupSO>();
        createdDialogues = new Dictionary<string, DSDialogueSO>();
        loadedGroups = new Dictionary<string, DSGroup>();
        loadedNodes = new Dictionary<string, DSNode>();
    }

    #region Save Methods
    public static void Save() {
        CreateStaticFolders();
        GetElementsFromGraphView();

        DSGraphSaveDataSO graphData = CreateAsset<DSGraphSaveDataSO>($"{editorFolderPath}/Graphs", 
            $"{graphFileName}Graph");

        graphData.Initialize(graphFileName);

        DSDialogueContainerSO dialogueContainer = CreateAsset<DSDialogueContainerSO>(containerFolderPath, graphFileName);

        dialogueContainer.Initialize(graphFileName);

        SaveGroups(graphData, dialogueContainer);
        SaveNoded(graphData, dialogueContainer);

        SaveAsset(graphData);
        SaveAsset(dialogueContainer);
    }

    #region Node Methods
    private static void SaveNoded(DSGraphSaveDataSO graphData, DSDialogueContainerSO dialogueContainer) {
        SerializableDictionary<string, List<string>> groupedNodeNames = new SerializableDictionary<string, List<string>>();
        List<string> ungroupedNodeNames = new List<string>();

        foreach (DSNode node in nodes) {
            SaveNodeToGraph(node, graphData);
            SaveNodeToScriptableObject(node, dialogueContainer);

            if (node.Group != null) {
                groupedNodeNames.AddItem(node.Group.title, node.DialogueName);
                continue;
            }

            ungroupedNodeNames.Add(node.DialogueName);
        }

        UpdateDialogueChoicesConnections();

        UpdateOldGroupedNodes(groupedNodeNames, graphData);
        UpdateOldUngroupedNodes(ungroupedNodeNames, graphData);
    }

    private static void SaveNodeToGraph(DSNode node, DSGraphSaveDataSO graphData) {
        List<DSChoiceSaveData> choices = CloneNodeChoices(node.Choices);

        DSNodeSaveData nodeData = new DSNodeSaveData() {
            ID = node.ID,
            Name = node.DialogueName,
            Choices = choices,
            Text = node.Text,
            GroupID = node.Group?.ID,
            DialogueType = node.DialogueType,
            Position = node.GetPosition().position
        };

        graphData.Nodes.Add(nodeData);
    }

    private static void SaveNodeToScriptableObject(DSNode node, DSDialogueContainerSO dialogueContainer) {
        DSDialogueSO dialogue;

        if (node.Group != null) {
            dialogue = CreateAsset<DSDialogueSO>($"{containerFolderPath}/Groups/{node.Group.title}/Dialogues", node.DialogueName);

            dialogueContainer.DialoguesGroups.AddItem(createdDialogueGroups[node.Group.ID], dialogue);
        }
        else {
            dialogue = CreateAsset<DSDialogueSO>($"{containerFolderPath}/Global/Dialogues", node.DialogueName);

            dialogueContainer.UngroupedDialogues.Add(dialogue);
        }

        dialogue.Initialize(
            node.DialogueName,
            node.Text,
            ConvertNodeChoicesToDialogueChoices(node.Choices),
            node.DialogueType,
            node.IsStartingNode()
        );

        createdDialogues.Add(node.ID, dialogue);
        SaveAsset(dialogue);
    }

    private static List<DSDialogueChoiceData> ConvertNodeChoicesToDialogueChoices(List<DSChoiceSaveData> nodeChoices) {
        List<DSDialogueChoiceData> dialogueChoices = new List<DSDialogueChoiceData>();

        foreach (DSChoiceSaveData nodeChoice in nodeChoices) {
            DSDialogueChoiceData choiceData = new DSDialogueChoiceData() { 
                Text = nodeChoice.Text
            };

            dialogueChoices.Add(choiceData);
        }

        return dialogueChoices;
    }

    private static void UpdateDialogueChoicesConnections() {
        foreach (DSNode node in nodes) {
            DSDialogueSO dialogue = createdDialogues[node.ID];

            for (int choiceIndex = 0; choiceIndex < node.Choices.Count; choiceIndex++) {
                DSChoiceSaveData nodeChoice = node.Choices[choiceIndex];

                if (string.IsNullOrEmpty(nodeChoice.NodeID))
                    continue;

                dialogue.Choices[choiceIndex].NextDialogue = createdDialogues[nodeChoice.NodeID];

                SaveAsset(dialogue);
            }
        }
    }

    private static void UpdateOldGroupedNodes(SerializableDictionary<string, List<string>> currentGroupedNodeNames, DSGraphSaveDataSO graphData) {
        if (graphData.OldGroupedNodeNames != null && graphData.OldGroupedNodeNames.Count != 0) {
            //List<string> nodesToRemove = graphData.OldGroupedNodeNames.Except(currentUngroupedNodeNames).ToList();

            foreach (KeyValuePair<string, List<string>> oldGroupNode in graphData.OldGroupedNodeNames) {
                List<string> nodesToRemove = new List<string>();

                if (currentGroupedNodeNames.ContainsKey(oldGroupNode.Key)) {
                    nodesToRemove = oldGroupNode.Value.Except(currentGroupedNodeNames[oldGroupNode.Key]).ToList();
                }

                foreach (string nodeToRemove in nodesToRemove) {
                    RemoveAsset($"{containerFolderPath}/Groups/{oldGroupNode.Key}/Dialogues", nodeToRemove);
                }
            }

        }

        graphData.OldGroupedNodeNames = new SerializableDictionary<string, List<string>>(currentGroupedNodeNames);
    }

    private static void UpdateOldUngroupedNodes(List<string> currentUngroupedNodeNames, DSGraphSaveDataSO graphData) {
        if (graphData.OldUngroupedNodeNames != null && graphData.OldUngroupedNodeNames.Count != 0) {
            List<string> nodesToRemove = graphData.OldUngroupedNodeNames.Except(currentUngroupedNodeNames).ToList();

            foreach (string nodeToRemove in nodesToRemove) {
                RemoveAsset($"{containerFolderPath}/Global/Dialogues", nodeToRemove);
            }
        }

        graphData.OldUngroupedNodeNames = new List<string>(currentUngroupedNodeNames);
    }
    #endregion

    #region Group Methods
    private static void SaveGroups(DSGraphSaveDataSO graphData, DSDialogueContainerSO dialogueContainer) {
        List<string> groupNames = new List<string>();

        foreach (DSGroup group in groups) {
            SaveGroupToGraph(group, graphData);
            SaveGroupToScriptableObjetc(group, dialogueContainer);

            groupNames.Add(group.title);
        }

        UpdateOldGroups(groupNames, graphData);
    }

    private static void SaveGroupToGraph(DSGroup group, DSGraphSaveDataSO graphData) {
        DSGroupSaveData groupData = new DSGroupSaveData() {
            ID = group.ID,
            Name = group.title,
            Position = group.GetPosition().position
        };

        graphData.Groups.Add(groupData);
    }

    private static void SaveGroupToScriptableObjetc(DSGroup group, DSDialogueContainerSO dialogueContainer) {
        string groupName = group.title;

        CreateFolder($"{containerFolderPath}/Groups", groupName);
        CreateFolder($"{containerFolderPath}/Groups/{groupName}", "Dialogues");

        DSDialogueGroupSO dialogueGroup = CreateAsset<DSDialogueGroupSO>($"{containerFolderPath}/Groups/{groupName}", groupName);
        dialogueGroup.Initialize(groupName);

        createdDialogueGroups.Add(group.ID, dialogueGroup);

        dialogueContainer.DialoguesGroups.Add(dialogueGroup, new List<DSDialogueSO>());

        SaveAsset(dialogueGroup);
    }

    private static void UpdateOldGroups(List<string> currentGroupsNames, DSGraphSaveDataSO graphData) {
        if (graphData.OldGroupNames != null && graphData.OldGroupNames.Count != 0) {
            List<string> groupsToRemove = graphData.OldGroupNames.Except(currentGroupsNames).ToList();

            foreach (string groupToRemove in groupsToRemove) {
                RemoveFolder($"{containerFolderPath}/Groups/{groupToRemove}");
            }
        }

        graphData.OldGroupNames = new List<string>(currentGroupsNames);
    }
    #endregion
    #endregion

    #region Load Methods
    public static void Load() {
        DSGraphSaveDataSO graphData = LoadAsset<DSGraphSaveDataSO>($"{editorFolderPath}/Graphs", graphFileName);

        if (graphData == null)  // Si la primera falla, se intenta cargar el archivo seleccionado desde la carpeta Examples.
            graphData = LoadAsset<DSGraphSaveDataSO>($"{editorFolderPath}/Graphs/Examples", graphFileName);

        if (graphData == null) {    // Si el archivo no esta en la carpeta Examples, entonces muestra error.
            EditorUtility.DisplayDialog(
                "No se ha podido cargar el archivo.",
                "No se ha podido encontrar el archivo en la siguiente ruta: \n\n" +
                $"{editorFolderPath}/Graphs/{graphFileName}. \n\n" +
                "Asegurese de elegir el archivo correcto, tambi�n debe estar colocado en la ruta especificada.",
                "Entendido"
            );
            return;
        }

        DSEditorWindow.UpdateFileName(graphData.FileName);

        LoadGroups(graphData.Groups);
        LoadNodes(graphData.Nodes);
        LoadNodesConnections();
    }

    #region Node Methods
    private static void LoadNodes(List<DSNodeSaveData> nodes) {
        foreach (DSNodeSaveData nodeData in nodes) {

            List<DSChoiceSaveData> choices = CloneNodeChoices(nodeData.Choices);

            DSNode node = graphView.CreateNode(nodeData.Name, nodeData.DialogueType, nodeData.Position, false);

            node.ID = nodeData.ID;
            node.Choices = choices;
            node.Text = nodeData.Text;

            node.Draw();
            graphView.AddElement(node);
            loadedNodes.Add(node.ID, node);

            if (string.IsNullOrEmpty(nodeData.GroupID))
                continue;

            DSGroup group = loadedGroups[nodeData.GroupID];
            node.Group = group;
            group.AddElement(node);
        }
    }
    private static void LoadNodesConnections() {
        foreach (KeyValuePair<string, DSNode> loadedNode in loadedNodes) {
            foreach (Port choicePort in loadedNode.Value.outputContainer.Children()) {
                DSChoiceSaveData choiceData = (DSChoiceSaveData)choicePort.userData;

                if (string.IsNullOrEmpty(choiceData.NodeID))
                    continue;

                DSNode nextNode = loadedNodes[choiceData.NodeID];
                Port nextNodeInputPort = (Port)nextNode.inputContainer.Children().First();

                Edge edge = choicePort.ConnectTo(nextNodeInputPort);
                graphView.AddElement(edge);

                loadedNode.Value.RefreshPorts();
            }
        }
    }
    #endregion

    #region Group Methods
    private static void LoadGroups(List<DSGroupSaveData> groups) {
        foreach (DSGroupSaveData groupData in groups) {
            DSGroup group = graphView.CreateGroup(groupData.Name, groupData.Position);

            group.ID = groupData.ID;

            loadedGroups.Add(group.ID, group);
        }
    }
    #endregion
    #endregion

    #region Creation Methods
    private static void CreateStaticFolders() {
        CreateFolder($"{editorFolderPath}", "Graphs");

        CreateFolder("{dsFatherFolderPath}", "DialogueSystem");
        //CreateFolder($"{dsFolderPath}", "GraphSaveData");
        CreateFolder($"{dsFolderPath}", "DialoguesSave");

        CreateFolder($"{dsFolderPath}/DialoguesSave", graphFileName);
        CreateFolder(containerFolderPath, "Global");
        CreateFolder(containerFolderPath, "Groups");
        CreateFolder($"{containerFolderPath}/Global", "Dialogues");
    }
    #endregion

    #region Fetch Methods
    private static void GetElementsFromGraphView() {
        Type groupType = typeof(DSGroup);

        graphView.graphElements.ForEach(graphElement => { 
            if (graphElement is DSNode node) {
                nodes.Add(node);
                return;
            }

            if (graphElement.GetType() == groupType) {
                DSGroup group = (DSGroup)graphElement;
                groups.Add(group);
                return;
            }
        });
    }
    #endregion

    #region Utility Methods
    public static void CreateFolder(string path, string folderName) {
        if (AssetDatabase.IsValidFolder($"{path}/{folderName}"))
            return;

        AssetDatabase.CreateFolder(path, folderName);
    }
    public static void RemoveFolder(string fullPath) {
        FileUtil.DeleteFileOrDirectory($"{fullPath}.meta");
        FileUtil.DeleteFileOrDirectory($"{fullPath}/");
    }
    public static T CreateAsset<T>(string path, string assetName) where T : ScriptableObject {
        string fullPath = $"{path}/{assetName}.asset";

        T asset = LoadAsset<T>(path, assetName);

        if (asset == null) {
            asset = ScriptableObject.CreateInstance<T>();

            AssetDatabase.CreateAsset(asset, fullPath);
        }



        return asset;
    }

    public static T LoadAsset<T>(string path, string assetName) where T : ScriptableObject {
        string fullPath = $"{path}/{assetName}.asset";

        return AssetDatabase.LoadAssetAtPath<T>(fullPath);
    }

    private static List<DSChoiceSaveData> CloneNodeChoices(List<DSChoiceSaveData> nodeChoices) {
        List<DSChoiceSaveData> choices = new List<DSChoiceSaveData>();

        foreach (DSChoiceSaveData choice in nodeChoices) {
            DSChoiceSaveData choiceData = new DSChoiceSaveData() {
                Text = choice.Text,
                NodeID = choice.NodeID
            };

            choices.Add(choiceData);
        }

        return choices;
    }
    public static void RemoveAsset(string path, string assetName) {
        AssetDatabase.DeleteAsset($"{path}/{assetName}.asset");
    }
    public static void SaveAsset(UnityEngine.Object asset) {
        EditorUtility.SetDirty(asset);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
    #endregion
}
