using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DSDialogue))]
public class DSInspector : Editor {

	/* Dialogue Scriptable Objects */
	private SerializedProperty dialogueContainerProperty;
	private SerializedProperty dialogueGroupProperty;
	private SerializedProperty dialogueProperty;

	/* Filters */
	private SerializedProperty groupedDialoguesProperty;
	private SerializedProperty startingDialoguesOnlyProperty;

	/* Indexe */
	private SerializedProperty selectedDialogueGroupIndexProperty;
	private SerializedProperty selectedDialogueIndexProperty;

	private void OnEnable() {
		dialogueContainerProperty = serializedObject.FindProperty("dialogueContainer");
		dialogueGroupProperty = serializedObject.FindProperty("dialogueGroup");
		dialogueProperty = serializedObject.FindProperty("dialogue");

		groupedDialoguesProperty = serializedObject.FindProperty("groupedDialogues");
		startingDialoguesOnlyProperty = serializedObject.FindProperty("startingDialoguesOnly");

		selectedDialogueGroupIndexProperty = serializedObject.FindProperty("selectedDialogueGroupIndex");
		selectedDialogueIndexProperty = serializedObject.FindProperty("selectedDialogueIndex");
    }

    public override void OnInspectorGUI() {
		serializedObject.Update();

		DrawDialogueContainerArea();

		DSDialogueContainerSO dialogueContainer = (DSDialogueContainerSO)dialogueContainerProperty.objectReferenceValue;

		if (dialogueContainer == null) {
            StopDrawing("Selecciona un archivo de di�logos para acceder al resto de variables.");
            return;
        }

        DrawFiltersArea();

		bool currentStartingDialoguesOnlyFilter = startingDialoguesOnlyProperty.boolValue;

		List<string> dialogueNames;

		string dialogueFolderPath = $"{DSIOUtility.dsFolderPath}/DialoguesSave/{dialogueContainer.FileName}";
		string dialogueInfoMessage;

		if (groupedDialoguesProperty.boolValue) {
			List<string> dialogueGroupNames = dialogueContainer.GetDialogueGroupNames();

			if (dialogueGroupNames.Count == 0) {
				StopDrawing("No hay grupos de di�logos en este archivo de di�logos.");
				return;
            }

			DrawDialogueGroupArea(dialogueContainer, dialogueGroupNames);

			DSDialogueGroupSO dialogueGroup = (DSDialogueGroupSO)dialogueGroupProperty.objectReferenceValue;

			dialogueNames = dialogueContainer.GetGroupedDialogueNames(dialogueGroup, currentStartingDialoguesOnlyFilter);

			dialogueFolderPath += $"/Groups/{dialogueGroup.GroupName}/Dialogues";

			dialogueInfoMessage = "No hay di�golos " + (currentStartingDialoguesOnlyFilter ? "iniciales " : "") 
				+ "en este grupo de di�logos.";
        }
		else {
			dialogueNames = dialogueContainer.GetUngroupedDialogueNames(currentStartingDialoguesOnlyFilter);

			dialogueFolderPath += $"/Global/Dialogues";

			dialogueInfoMessage = "No hay di�golos " + (currentStartingDialoguesOnlyFilter ? "iniciales " : "") 
				+ "desagrupados en este archivo de di�logos.";
		}

		if (dialogueNames.Count == 0) {
			StopDrawing(dialogueInfoMessage);
			return;
        }

		DrawDialogueArea(dialogueNames, dialogueFolderPath);

		serializedObject.ApplyModifiedProperties();
    }

    #region Draw Methods
    private void DrawDialogueContainerArea() {
        DSInspectorUtility.DrawHeader("Dialogue Container");
		dialogueContainerProperty.DrawPropertyField();

		DSInspectorUtility.DrawSpace();
	}
	private void DrawFiltersArea() {
		DSInspectorUtility.DrawHeader("Filters");
		groupedDialoguesProperty.DrawPropertyField();
		startingDialoguesOnlyProperty.DrawPropertyField();

		DSInspectorUtility.DrawSpace(8);
	}
	private void DrawDialogueGroupArea(DSDialogueContainerSO dialogueContainer, List<string> dialogueGroupNames) {
        DSInspectorUtility.DrawHeader("Dialogue Group");

        int oldSelectedDialogueGroupIndex = selectedDialogueGroupIndexProperty.intValue;

        DSDialogueGroupSO oldDialogueGroup = (DSDialogueGroupSO)dialogueGroupProperty.objectReferenceValue;

		bool isOldDialogueGroupNull = oldDialogueGroup == null;

		string oldDialogueGroupName = isOldDialogueGroupNull ? "" : oldDialogueGroup.GroupName;

		UpdateIndexOnNameListUpdate(dialogueGroupNames, selectedDialogueGroupIndexProperty, oldSelectedDialogueGroupIndex, 
			oldDialogueGroupName, isOldDialogueGroupNull);

        selectedDialogueGroupIndexProperty.intValue = DSInspectorUtility.DrawPopup("Dialogue Group",
            selectedDialogueGroupIndexProperty, dialogueGroupNames.ToArray());

        string selectedDialogueGroupName = dialogueGroupNames[selectedDialogueGroupIndexProperty.intValue];

        DSDialogueGroupSO selectedDialogueGroup = DSIOUtility.LoadAsset<DSDialogueGroupSO>(
            $"{DSIOUtility.dsFolderPath}/DialoguesSave/{dialogueContainer.FileName}/Groups/{selectedDialogueGroupName}",
            selectedDialogueGroupName);

        dialogueGroupProperty.objectReferenceValue = selectedDialogueGroup;

        DSInspectorUtility.DrawDisabledFields(() => dialogueGroupProperty.DrawPropertyField());

        DSInspectorUtility.DrawSpace();
    }

    private void DrawDialogueArea(List<string> dialogueNames, string dialogueFolderPath) {
		DSInspectorUtility.DrawHeader("Dialogue");

		DSDialogueSO oldDialogue = (DSDialogueSO)dialogueProperty.objectReferenceValue;

		int oldSelectedDialogueIndex = selectedDialogueIndexProperty.intValue;

		bool isOldDialogueNull = oldDialogue == null;

		string oldDialogueName = isOldDialogueNull ? "" : oldDialogue.DialogueName;

		UpdateIndexOnNameListUpdate(dialogueNames, selectedDialogueIndexProperty, 
			oldSelectedDialogueIndex, oldDialogueName, isOldDialogueNull);

		selectedDialogueIndexProperty.intValue = DSInspectorUtility.DrawPopup("Dialogue", 
			selectedDialogueIndexProperty, dialogueNames.ToArray());

		string selectedDialogueName = dialogueNames[selectedDialogueIndexProperty.intValue];

		DSDialogueSO selectedDialogue = DSIOUtility.LoadAsset<DSDialogueSO>(dialogueFolderPath, selectedDialogueName);

		dialogueProperty.objectReferenceValue = selectedDialogue;

        DSInspectorUtility.DrawDisabledFields(() => dialogueProperty.DrawPropertyField());
		
	}
	private void StopDrawing(string reason, MessageType messageType=MessageType.Info) {
		DSInspectorUtility.DrawHelpBox(reason, messageType);

		DSInspectorUtility.DrawSpace();

		DSInspectorUtility.DrawHelpBox("Debes seleccionar un di�logo para que este componente\n " +
			"funcione correctamente en tiempo de ejecuci�n.", MessageType.Warning);

		serializedObject.ApplyModifiedProperties();
	}
	#endregion

	#region Index Methods
	private void UpdateIndexOnNameListUpdate(List<string> optionNames, SerializedProperty indexProperty, 
		int oldSelectedPropertyIndex, string oldPropertyName, bool isOldPropertyNull) {

		if (isOldPropertyNull) {
			indexProperty.intValue = 0;
			return;
        }

		bool oldIndexIsOutOfBoundsOfNamesListCount = oldSelectedPropertyIndex > optionNames.Count - 1;
		bool oldNameIsDifferentThanSelectedName = oldIndexIsOutOfBoundsOfNamesListCount || 
			oldPropertyName != optionNames[oldSelectedPropertyIndex];

		if (oldNameIsDifferentThanSelectedName)
				if (optionNames.Contains(oldPropertyName))
					indexProperty.intValue = optionNames.IndexOf(oldPropertyName);
				else
					indexProperty.intValue = 0;
	}
	#endregion
}
