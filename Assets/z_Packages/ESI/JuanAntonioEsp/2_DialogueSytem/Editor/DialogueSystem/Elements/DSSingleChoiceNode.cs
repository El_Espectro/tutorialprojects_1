using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class DSSingleChoiceNode : DSNode {
    public override void Initialize(string nodeName, DSGraphView dSGraphView, Vector2 position) {
        base.Initialize(nodeName, dSGraphView, position);

        DialogueType = DSDialogueType.SingleChoice;

        DSChoiceSaveData choiceData = new DSChoiceSaveData() {
            Text = "Next Dialogue"
        };

        Choices.Add(choiceData);
    }
    public override void Draw() {
        base.Draw();

        /* Output Container */
        foreach (DSChoiceSaveData choice in Choices) {
            Port choicePort = this.CreatePort(choice.Text);

            choicePort.userData = choice;

            outputContainer.Add(choicePort);
        }

        RefreshExpandedState();
    }
}
