using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PauseMenuManager : MonoBehaviour {

    public UIActions InputActions { get; private set; }
    public UIActions.PauseMenuActions PauseActions { get; private set; }

    public static bool onPause { get; private set; }

    // Placeholders
    private List<AudioSource> audios = new List<AudioSource>();

    private void Awake() {
        InputActions = new UIActions();
        PauseActions = InputActions.PauseMenu;
    }

    private void OnEnable() {
        InputActions.Enable();
        AddInputActionsCallbacks();
    }

    private void OnDisable() {
        InputActions.Disable();
        RemoveInputActionsCallbacks();
    }

    // Start is called before the first frame update
    void Start() {
        #region Testing
        /*audios.Add(new AudioSource());
        StartCoroutine(StartLoops());*/
        #endregion
    }

    // Update is called once per frame
    void Update() {
        
    }

    void PauseResumeGame(InputAction.CallbackContext context) {
        if (!onPause) {
            onPause = true;
            Time.timeScale = 0f;
            ThingsToBlockOnPause();
        }
        else {
            Time.timeScale = 1f;
            ThingsToUnblockAferPause();
            onPause = false;
        }
    }

    private void ThingsToBlockOnPause() {
        TestPlayer.playerActions.Disable();
        AudioListener.pause = true;
    }

    private void ThingsToUnblockAferPause() {
        TestPlayer.playerActions.Enable();
        AudioListener.pause = false;
    }

    private IEnumerator StartLoops() {
        // Se marcan los audios especificados para que no dejen de sonar cuando el juego se detenga.
        foreach (AudioSource audio in audios) {
            audio.ignoreListenerPause = true;
        }

        yield return null;
    }

    #region Input Methods
    private void AddInputActionsCallbacks() {
        PauseActions.Pause.performed += PauseResumeGame;
    }
    private void RemoveInputActionsCallbacks() {
        PauseActions.Pause.performed -= PauseResumeGame;
    }
    #endregion
}
