using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[Serializable]
public class GameSettings {

	/*[field: SerializeField] */public bool fullScreen/* { get; set; }*/;
	/*[field: SerializeField] */public int textureQuality/* { get; set; }*/;
	/*[field: SerializeField] */public int antialiasing/* { get; set; }*/;
	/*[field: SerializeField] */public int vSync/* { get; set; }*/;
	/*[field: SerializeField] */public int resolutionIndex/* { get; set; }*/;
	/*[field: SerializeField] */public float audioVolume/* { get; set; }*/;

	/*public GameSettings() {
		fullScreen = true;
		textureQuality = 0;
		antialiasing = 4;
		vSync = 1;
		resolutionIndex = 0;
		audioVolume = 0.5f;
    }*/

	public int GetAntialiasingOption() {
        int index = 0;
        switch (antialiasing) {
            case 0:
                index = 0;
                break;
            case 2:
                index = 1;
                break;
            case 4:
                index = 2;
                break;
            case 8:
                index = 3;
                break;
            default:
                index = 4;
                break;
        }

        return index;
    }
}
