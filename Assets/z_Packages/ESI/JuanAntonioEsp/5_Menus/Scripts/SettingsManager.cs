using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour {

    [HideInInspector] public GameSettings gameSettings;
    private string filePath;
    
    public Toggle fullScreenToggle;
    public Dropdown resolutionDropdown;
    public Dropdown textureQualityDropdown;
    public Dropdown antialiasingDropdown;
    public Dropdown vSyncDropdown;
    public Slider audioVolumeSlider;
    public Button applyButton;

    public Resolution[] resolutions;

    private void Awake() {
        resolutions = Screen.resolutions;
        filePath = $"{Application.persistentDataPath}/gameSettings.json";
    }

    private void OnEnable() {
        if (gameSettings == null)
            gameSettings = new GameSettings();

        fullScreenToggle.onValueChanged.AddListener(delegate { OnFullScreenToggle(); });
        resolutionDropdown.onValueChanged.AddListener(delegate { OnResolutionChange(); });
        textureQualityDropdown.onValueChanged.AddListener(delegate { OnTextureQualityChange(); });
        antialiasingDropdown.onValueChanged.AddListener(delegate { OnAntialiasingChange(); });
        vSyncDropdown.onValueChanged.AddListener(delegate { OnVSyncChange(); });
        audioVolumeSlider.onValueChanged.AddListener(delegate { OnAudioVolumeChange(); });
        applyButton.onClick.AddListener(delegate { OnApplyButtonClick(); });
    }

    // Start is called before the first frame update
    void Start() {
        gameSettings.fullScreen = fullScreenToggle.isOn = Screen.fullScreen;

        gameSettings.textureQuality = textureQualityDropdown.value = QualitySettings.masterTextureLimit;
        gameSettings.antialiasing = antialiasingDropdown.value = QualitySettings.antiAliasing;
        gameSettings.vSync = vSyncDropdown.value = QualitySettings.vSyncCount;
        //gameSettings.audioVolume = audioVolumeSlider.value = 0.5f;

        StartCoroutine(StartSettings());
    }

    // Update is called once per frame
    void Update() {
        
    }

    #region Settings Methods
    public void OnFullScreenToggle() {
        Screen.fullScreen = gameSettings.fullScreen = fullScreenToggle.isOn;
    }

    public void OnResolutionChange() {
        Resolution resolution = resolutions[resolutionDropdown.value];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        gameSettings.resolutionIndex = resolutionDropdown.value;
    }

    public void OnTextureQualityChange() {
        QualitySettings.masterTextureLimit = gameSettings.textureQuality = textureQualityDropdown.value;
    }

    public void OnAntialiasingChange() {
        switch (antialiasingDropdown.value) {
            case 0:
                QualitySettings.antiAliasing = gameSettings.antialiasing = 0;
                break;
            case 1:
                QualitySettings.antiAliasing = gameSettings.antialiasing = 2;
                break;
            case 2:
                QualitySettings.antiAliasing = gameSettings.antialiasing = 4;
                break;
            case 3:
                QualitySettings.antiAliasing = gameSettings.antialiasing = 8;
                break;
            default:
                Debug.LogWarning("Unkown Antialiasing Option");
                break;
        }
        //QualitySettings.antiAliasing = gameSettings.antialiasing = (int) Mathf.Pow(2f, antialiasingDropdown.value);
        //gameSettings.antialiasing = antialiasingDropdown.value;
    }

    public void OnVSyncChange() {
        QualitySettings.vSyncCount = gameSettings.vSync = vSyncDropdown.value;
    }

    public void OnAudioVolumeChange() {
        // ToDo: Asignar a un Mixer o algo para que gestione el audio.
        gameSettings.audioVolume = audioVolumeSlider.value;
    }

    public void OnApplyButtonClick() {
        SaveSettings();
    }

    public void SaveSettings() {
        string jsonData = JsonUtility.ToJson(gameSettings, true);
        File.WriteAllText(filePath, jsonData);

    }

    public void LoadSettings() {
        if (!File.Exists(filePath)) {
            Debug.LogWarning("El archivo de ajustes en \"" + filePath + "\" no existe.");
            return;
        }

        gameSettings = JsonUtility.FromJson<GameSettings>(File.ReadAllText(filePath));

        fullScreenToggle.isOn = gameSettings.fullScreen;
        resolutionDropdown.value = gameSettings.resolutionIndex;
        textureQualityDropdown.value = gameSettings.textureQuality;
        antialiasingDropdown.value = gameSettings.GetAntialiasingOption();
        vSyncDropdown.value = gameSettings.vSync;
        audioVolumeSlider.value = gameSettings.audioVolume;
    }
    #endregion

    #region Coroutine Methods
    private IEnumerator StartSettings() {
        foreach (Resolution resolution in resolutions) {
            resolutionDropdown.options.Add(new Dropdown.OptionData(resolution.ToString()));
        }

        yield return null;

        LoadSettings();

        resolutionDropdown.RefreshShownValue();
    }
    #endregion
}
