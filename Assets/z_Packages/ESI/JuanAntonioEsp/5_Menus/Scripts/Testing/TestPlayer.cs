using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlayer : MonoBehaviour {

    public static TestPlayerActions playerActions { get; private set; }

    private void Awake() {
        playerActions = new TestPlayerActions();
    }

    private void OnEnable() {
        playerActions.Enable();
        playerActions.PlayerDefault.Enable();
    }

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }

    private void OnDisable() {
        playerActions.Disable();
        playerActions.PlayerDefault.Disable();
    }
}
